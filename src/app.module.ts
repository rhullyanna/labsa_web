import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
//import { typeOrmConfig } from './config/typeorm.config';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
	imports: [
		ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env.local' }),
		//TypeOrmModule.forRoot(typeOrmConfig)
		TypeOrmModule.forRootAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: async (configService: ConfigService) => ({
				type: 'postgres' as 'postgres',
				host: configService.get('DATABASE_HOST', 'localhost'),
				port: configService.get<number>('DATABASE_PORT', 5432),
				username: configService.get('DATABASE_USER', 'postgres'),
				password: configService.get('DATABASE_PASS', 'postgres'),
				database: configService.get('DATABASE_SCHEMA', 'db'),
				entities: configService.get(__dirname + 'TYPEORM'),
				sync: configService.get('DATABASE_SYNC', false),
			}),
		}),
	],
	controllers: [],
	providers: [],
})
export class AppModule {}
