FROM node:lts-alpine3.9 As development

WORKDIR /opt/app-root/src

COPY package*.json ./

RUN yarn --only=development

COPY . .

RUN yarn run build

FROM node:lts-alpine3.9 as production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /opt/app-root/src

COPY package*.json ./

RUN yarn --only=production

COPY . .

COPY --from=development /opt/app-root/src/dist ./dist

CMD ["node", "dist/main"]